class PagesController {
  home = (req, res, next) => {
    const title = "Hello World",
      subTitle = "Selamat datang Dunia";

    res.render("index", { title, subTitle });
  };
}

module.exports = PagesController;
