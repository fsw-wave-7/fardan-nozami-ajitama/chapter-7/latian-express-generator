const router = require("express").Router();
const Page = require("../controllers/PagesController");

const page = new Page();

router.get("/", page.home);

module.exports = router;
